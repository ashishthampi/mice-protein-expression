This is a clustering problem which can be solved using an Unsupervised Machine Learning algorithm.

The data is taken from a mice sample which is clustered as 8 classes. The features of the data set is the expression level of 77 proteins of each mouse sample. 
The aim of the project is to clusterize the mice samples as the following classes.
* c-CS-s: control mice, stimulated to learn, injected with saline (9 mice)

* c-CS-m: control mice, stimulated to learn, injected with memantine (10 mice)

* c-SC-s: control mice, not stimulated to learn, injected with saline (9 mice)

* c-SC-m: control mice, not stimulated to learn, injected with memantine (10 mice)

* t-CS-s: trisomy mice, stimulated to learn, injected with saline (7 mice)

* t-CS-m: trisomy mice, stimulated to learn, injected with memantine (9 mice)

* t-SC-s: trisomy mice, not stimulated to learn, injected with saline (9 mice)

* t-SC-m: trisomy mice, not stimulated to learn, injected with memantine (9 mice)

Here in this project we use Principal Componet Analysis (PCA) and KMeans clustering for an accurate prediction.
This repository contains
* - [README](Readme.md)
* - [Data Set](Data_Cortex_Nuclear.csv)
* - [Notebook](MouseProteinExpression.ipynb)
